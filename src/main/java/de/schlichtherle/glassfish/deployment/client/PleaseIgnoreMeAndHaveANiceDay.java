/*
 * Copyright (C) 2013 Schlichtherle IT Services
 * All rights reserved. Use is subject to license terms.
 */
package de.schlichtherle.glassfish.deployment.client;

/**
 * This class solely exists to meet the requirements of Maven Central that
 * any project needs to have some source and Javadoc JARs attached.
 * However, this shouldn't be required here because this project just
 * repackages the Deployment Client JAR for the Oracle GlassFish server.
 * So please ignore this class and have a nice day!
 *
 * @author Christian Schlichtherle
 */
public class PleaseIgnoreMeAndHaveANiceDay {
    private PleaseIgnoreMeAndHaveANiceDay() { }
}
